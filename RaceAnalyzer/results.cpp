
// Tracey DeCorte
// CST420
// Assignment 3
// results.cpp



#include <numeric>

#include "mainDisplay.h"
#include "results.h"






// Returns a pair of rider name and seconds from rider parameter


RaceAnalyzer::PairResults RiderResultTimes::operator() ( Rider const &rider ) const
{
	RaceAnalyzer::PairResults pair;
	RaceAnalyzer::Results results;

	string riderName = rider.getRiderName();
	string teamName = rider.getTeamName();
	string countryName = rider.getCountry();


	vector<int> seconds = rider.getSeconds();


	int totalSeconds = 0;
	int prevStageSecs = 0;


	if ( m_stage == 0 ) 
	{
		totalSeconds = accumulate( seconds.begin(), 
			seconds.end(), 
			totalSeconds );

		if ( m_team == teamName || m_team == "" )
		{
			if ( m_country == countryName || m_country == "" )
				pair = make_pair(totalSeconds, riderName);
		}
	}	


	else
	{
		totalSeconds = seconds[m_stage - 1];

		if ( m_team == teamName || m_team == "" )
		{
			if ( m_country == countryName || m_country == "" )
				pair = make_pair( totalSeconds, riderName );
		}
	}

	return pair;
}






// Checks if rider passed in as parameter is valid


bool RiderResults::operator() ( Rider const &rider ) const
{

	string riderName = rider.getRiderName();
	string teamName = rider.getTeamName();
	string countryName = rider.getCountry();

	vector<int> seconds = rider.getSeconds();



	if ( m_stage == 0 ) 
	{
		if ( m_team == teamName || m_team == "" )
		{
			if ( m_country == countryName || m_country == "" )
				return true;
		}
	}	


	else
	{
		if ( m_team == teamName || m_team == "" )
		{
			if ( m_country == countryName || m_country == "" )
				return true;
		}
	}


	return false;
}