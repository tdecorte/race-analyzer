
// Tracey DeCorte
// CST420
// Assignment 3
// stage.h



#ifndef stage_H
#define stage_H


#include <iterator>
#include <vector>


using namespace std; 






typedef vector<double> StageLengthVect;





// Container to hold data associated with the stages of the race

class Stages 
{

private:

	typedef vector<Stages> Distances;

	string m_stageLength;
	size_t m_numStages; 
	StageLengthVect m_lengthsStages;


public:	

	const size_t size() const { return m_numStages; }

	void setNumStages( int stages ) { m_numStages = stages; }
	void setStageLengths( StageLengthVect vect ); 

	StageLengthVect getLengths() const { return m_lengthsStages; } 

};




#endif


