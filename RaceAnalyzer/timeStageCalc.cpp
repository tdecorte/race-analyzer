
// Tracey DeCorte
// CST420
// Assignment 3
// timeStageCalc.cpp



#include <algorithm>
#include <numeric>

#include  "mainDisplay.h"
#include  "timeStageCalc.h"


using  namespace  std;








// Calculates seconds according to team name and stage specified


double calcSeconds( int stage, int totalSeconds, string m_teamName, RiderSet m_ridersSet, int m_numRiders )
{

	double seconds = 0;		

	list<int> riderTimes;

	RiderSet set;


	RiderTimes ridersTimes( m_teamName );

	copy_if( m_ridersSet.begin(), 
		m_ridersSet.end(), 
		inserter( set, set.begin() ), 
		ridersTimes );



	RiderTimesStage riderStage( stage );

	transform( set.begin(), 
		set.end(), 
		inserter( riderTimes, riderTimes.begin() ), 
		riderStage );



	riderTimes.sort();

	TotalTime times;

	copy( riderTimes.begin(), 
		riderTimes.end(), 
		back_inserter(times) );


	unsigned numIter = 0;

	return seconds = accumulate( times.begin(), 
		times.begin() + m_numRiders, 
		seconds );

}






// Returns number of seconds for stage


double TimeStages::operator() ( double time ) const
{

	static int stage = m_stage;
	int totalSeconds = 0;

	stage %= m_numStages;

	if ( stage == 0 )
		totalSeconds = 0;

	double seconds = calcSeconds( stage, 
		totalSeconds, 
		m_teamName, 
		m_ridersSet, 
		m_numRiders );

	stage++;

	return seconds;
}






// Calculates and returns seconds


double TimeStage::operator() ( double time ) const
{
	int totalSeconds = 0;

	double seconds = calcSeconds( m_stage, 
		totalSeconds, 
		m_teamName, 
		m_ridersSet, 
		m_numRiders );

	return seconds;
}