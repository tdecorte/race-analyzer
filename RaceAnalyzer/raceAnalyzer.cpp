
// Tracey DeCorte
// CST420
// Assignment 3
// raceAnalyzer.cpp



#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <sstream>


#include "raceAnalyzer.h"
#include "results.h"
#include "rider.h"
#include "timeStageCalc.h"


#define  SECS_PER_HOUR  3600
#define  SECS_PER_MIN   60





double toDouble( string length )
{
	return atof( length.c_str() );
}



// Reads files in and assigns data to respective classes 


RaceAnalyzer::RaceAnalyzer( const string  &stageFilename,
	const string  &riderFilename )
{


	ifstream infileStage( stageFilename );
	ifstream infileRider( riderFilename );

	if ( !infileStage || !infileRider )
	{
		cout << "File open failed.";
		exit( EXIT_FAILURE );
	}



	Rider::RiderInIter inIter( infileRider );	

	copy( inIter, Rider::RiderInIter(), inserter( m_ridersSet, m_ridersSet.begin() ) );



	InIterStage inIterStage( infileStage );

	StageLengths stageLengths;
	copy( inIterStage, InIterStage(), back_inserter(stageLengths) );


	StageLengthVect lengths;

	transform( stageLengths.begin(), stageLengths.end(), back_inserter(lengths), toDouble );




	m_stageDistances.setStageLengths( lengths );
	m_stageDistances.setNumStages( lengths.size() );

}






// Returns riders and stage/race times based on the
// criteria specified by the parameters.


RaceAnalyzer::Results RaceAnalyzer::riderResults( unsigned stage, 
	const string &team,
	const string  &country ) const
{

	RaceAnalyzer::Results results;

	RiderSet riderSet;
	RiderResults getRiders( stage, team, country );

	copy_if( m_ridersSet.begin(), m_ridersSet.end(), inserter( riderSet, riderSet.begin() ), getRiders );
	

	RiderResultTimes getResults( stage, team, country );
	transform( riderSet.begin(), riderSet.end(), back_inserter(results), getResults );


	results.sort();
	
	return results;
}






// Returns the names of all the countries represented
// in the race.


string getCountries( Rider rider )
{
	return rider.getCountry();
}


RaceAnalyzer::StrSet  RaceAnalyzer::countries()  const
{
	StrSet countries;
	RaceAnalyzer::Results::iterator iter;

	transform( m_ridersSet.begin(), m_ridersSet.end(), 
		inserter( countries, countries.begin() ), getCountries );

	return countries;
}






// Returns the names of all the teams in the race.


string getTeams( Rider rider )
{
	return rider.getTeamName();
}


RaceAnalyzer::StrSet  RaceAnalyzer::teams()  const
{
	StrSet namesTeams;
	RaceAnalyzer::Results::iterator iter;

	transform( m_ridersSet.begin(), m_ridersSet.end(), 
		inserter( namesTeams, namesTeams.begin() ), getTeams );

	return namesTeams;
}






// Converts the time for a stage/race into miles-per-hour.


RaceAnalyzer::MPH  RaceAnalyzer::calcMPH( Seconds  seconds, unsigned  stage )  const
{
	MPH mph = 0;
	MILES stageMiles = 0;

	StageLengthVect stageLengths = m_stageDistances.getLengths();


	if (stage == 0)
	{		
		stageMiles = accumulate( stageLengths.begin(), stageLengths.end(), 0.0 );
	}

	else
	{		
		stageMiles = stageLengths.at(stage-1);
	}


	unsigned  hours  =  seconds / SECS_PER_HOUR;
	seconds  %=  SECS_PER_HOUR;

	unsigned  minutes  =  seconds / SECS_PER_MIN;
	seconds  %=  SECS_PER_MIN;


	MPH mins = minutes/MPH(SECS_PER_MIN);
	MPH secs = seconds/MPH(SECS_PER_HOUR);
	MPH hour = hours + mins + secs;

	mph = stageMiles / hour;


	return mph;
}






// Returns the country name for a specified rider.


string  RaceAnalyzer::getCountry( const string  &riderName )  const
{	
	RiderIter iter = find( m_ridersSet.begin(), m_ridersSet.end(), riderName );
	Rider rider = *iter;

	return rider.getCountry();
}






// Returns the team name for a specified rider.


string  RaceAnalyzer::getTeam( const  string  &riderName )  const
{
	RiderIter iter = find( m_ridersSet.begin(), m_ridersSet.end(), riderName );
	Rider rider = *iter;

	return rider.getTeamName();
}






// Returns stage/race time for the specified team/stage. A
// team time for a stage is sum of the numRiders fastest
// times for the team.      


Seconds  RaceAnalyzer::teamTime( 
	const string  &teamName,
	unsigned stage,
	unsigned numRiders )  const
{

	Seconds seconds = 0;
	Seconds totalTime = 0;

	StageLengthVect lengthVect = m_stageDistances.getLengths();

	int numStages = this->numStages();

	TotalTime totalTimeVect;


	if (stage == 0)
	{
		stage = 0;
		RiderSet set;


		TimeStages teamTimes( teamName, stage, numRiders, m_ridersSet, numStages );
		
		totalTime = 0;
		
		transform( lengthVect.begin(), 
			lengthVect.end(), 
			back_inserter( totalTimeVect ), 
			teamTimes );

		totalTime = accumulate( totalTimeVect.begin(), 
			totalTimeVect.end(), 
			totalTime );

	}


	else if (stage != 0)
	{
		stage -= 1;
		TimeStage teamTime( teamName, stage, numRiders, m_ridersSet, numStages );


		transform( lengthVect.begin(), 
			lengthVect.begin() + 1, 
			back_inserter( totalTimeVect ), 
			teamTime );

		totalTime = accumulate( totalTimeVect.begin(), 
			totalTimeVect.end(), 
			totalTime );
	}


	return totalTime;
}

