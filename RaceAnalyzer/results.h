
// Tracey DeCorte
// CST420
// Assignment 3
// results.h



#ifndef results_H
#define results_H





// Functor associated with returning a Rider to be included in results.
// A pair of seconds and rider name is returned for Rider matching criteria 


class RiderResultTimes
{
	unsigned m_stage;
	string m_team;
	string m_country;

public:

	RiderResultTimes( unsigned stage, const string &team, const string &country ) : 

			m_stage( stage ), 
			m_team( team ), 
			m_country( country ) {}

	
			
	RaceAnalyzer::PairResults operator() ( Rider const &rider ) const;

};






// Functor associated with determining whether a Rider should be included in results
// Determines if rider should be included based on team name, country and stage


class RiderResults
{
	unsigned m_stage;
	string m_team;
	string m_country;

public:

	RiderResults( unsigned stage, const string &team, const string &country ) : 

		  m_stage( stage ), 
		  m_team( team ), 
		  m_country( country ) {}


	bool operator() ( Rider const &rider ) const;
};





#endif