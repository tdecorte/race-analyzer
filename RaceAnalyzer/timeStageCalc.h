
// Tracey DeCorte
// CST420
// Assignment 3
// timeStageCalc.h



#ifndef timeStageCalc_H
#define timeStageCalc_H






// Functor that determines whether the rider belongs to team specified

class RiderTimes
{
	string m_teamName;

public:

	RiderTimes( const string &teamName ) : m_teamName( teamName ) {}

	bool operator() ( Rider const &rider ) const 
	{

		if ( rider.getTeamName() == m_teamName )
			return true;

		else return false;
	}

};





// Functor that returns seconds for stage specified

class RiderTimesStage
{
	int m_stage;

public:

	RiderTimesStage( int stage ) : m_stage( stage ) {}


	int operator() ( Rider const &rider ) const
	{
		return rider.getSeconds().at(m_stage);
	}
};






extern double calcSeconds( int stage, int totalSeconds, string m_teamName,
	RiderSet m_ridersSet, int m_numRiders );






// Functor that calculates and returns the number of seconds for all stages

class TimeStages
{

	string m_teamName;
	unsigned m_stage;
	unsigned m_numRiders;
	int m_numStages;
	RiderSet m_ridersSet;
	

public:


	TimeStages( const string &teamName, unsigned &stage, unsigned numRiders, 
		RiderSet riders, int numStages ) : 

			m_teamName( teamName ), 
			m_stage( stage ), 
			m_numRiders( numRiders ), 
			m_ridersSet( riders ), 
			m_numStages( numStages ) {}


	double operator() ( double time ) const;

};






// Functor that calculates and returns the number of seconds for the stage specified

class TimeStage
{

	string m_teamName;
	unsigned m_stage;
	unsigned m_numRiders;
	int m_numStages;
	RiderSet m_ridersSet;


public:


	TimeStage( const string &teamName, unsigned &stage, unsigned numRiders, 
		RiderSet riders, int numStages ) : 

		m_teamName( teamName ), 
		m_stage( stage ), 
		m_numRiders( numRiders ), 
		m_ridersSet( riders ), 
		m_numStages( numStages ) {}


	double operator() ( double time ) const;

};


#endif