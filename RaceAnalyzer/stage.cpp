
// Tracey DeCorte
// CST420
// Assignment 3
// stage.cpp


#include "stage.h"




void Stages::setStageLengths( StageLengthVect vect ) 
{ 
	copy( vect.begin(), 
		vect.end(), 
		inserter( m_lengthsStages, m_lengthsStages.begin() ) ); 
}
