
// Tracey DeCorte
// CST420
// Assignment 3
// rider.h



#ifndef rider_H
#define rider_H


#include <string>
#include <set>
#include <vector>
#include <iterator>

using namespace std;






// Container to hold data associated with the riders of the race


class Rider
{
	friend  istream  &operator>>( istream  &lhs, Rider &rhs );

private:	

	string m_riderName;
	string m_country;
	string m_teamName;
	vector<int> m_seconds;	

public:
	
	typedef set<Rider> RiderSet;
	typedef RiderSet::iterator RiderIter;
	typedef istream_iterator<Rider> RiderInIter;   
	
	typedef vector<int> SecondSet;
	typedef SecondSet::iterator SecondIter;


	string getRiderName() const { return m_riderName; }
	string getCountry() const { return m_country; }
	string getTeamName() const { return m_teamName; }
	vector<int> getSeconds() const { return m_seconds; }
	
};

	

	bool operator< ( const Rider &rider1, const Rider &rider2 );	
	bool operator== ( const Rider &rider1, const string &riderName );

		






#endif