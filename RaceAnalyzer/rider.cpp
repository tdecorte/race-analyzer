
// Tracey DeCorte
// CST420
// Assignment 3
// rider.cpp



#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>


#include "rider.h"


using namespace std;




bool operator< (const Rider &rider1, const Rider &rider2)
{
	return rider1.getRiderName() < rider2.getRiderName();
} 



bool operator== ( const Rider &rider1, const string &riderName )
{
	return rider1.getRiderName() == riderName;
}



int toInt( string seconds )
{
	return atoi( seconds.c_str() );
}



// Stream extraction

istream  &operator>>( istream  &lhs, Rider &rhs )
{

	lhs  >>  rhs.m_riderName;
	lhs	 >>  rhs.m_country; 
	lhs	 >>  rhs.m_teamName;

	
	string riderTimes; 	
	getline( lhs, riderTimes );

	istringstream stream( riderTimes );
	
	vector<string> tempSeconds;

	copy( istream_iterator<string>(stream),
				istream_iterator<string>(),
				back_inserter( tempSeconds ) );
	
	rhs.m_seconds.clear();
	
	transform( tempSeconds.begin(), tempSeconds.end(), back_inserter(rhs.m_seconds), toInt );

	return lhs;

}
 







